# RevealIf
## Usage
```javascript
Reveal.initialize({
    If: {
        variables: {
            os: {type: "radio", ask: "使用しているOSを選択してください", options: [
                {value: "windows10", display: "Windows10"},
                {value: "windows11", display: "Windows11"},
                {value: "unix", display: "unix系(macOS等)"},
            ]},
            terminalType: {type: "select", ask: "スタートを右クリックした時に以下のどちらが表示されましたか？", options: [
                {value: "powershell", display: "PowerShell"},
                {value: "windowsTerminal", display: "ターミナルまたはTerminal"},
            ]},
        },
        shortcuts: {
            isWindows: {
                "type": "operation",
                    "operation": "||",
                    "values": [
                    {"type": "simple", "conditions": {"os": "windows10"}},
                    {"type": "simple", "conditions": {"os": "windows11"}}
                ]
            }
        }
    },
    plugins: [If],
});
```

変数の設定を行います
```html
<section>
    <div data-if-set="os"></div>
</section>
```

変数によって表示する内容を変えます  
`data-if`が要素内にあり、子要素に`data-if-condition`が含まれている時表示内容の変更を行います  
条件は要素内の最初のものが使用されます。  
使用する変数が定義されていない場合自動的に設定を促します。
```html
<section data-if>
    <textarea data-if-conditions>
        {"type": "simple", "conditions": {"os": "windows10"}}
    </textarea>
    windows10
</section>
<section data-if>
    <textarea data-if-conditions>
        {"type": "shortcut", "name": "isWindows"}
    </textarea>
    any Windows
</section>
```

`data-if-condition`内の要素はJSON形式で記述した論理式です  
