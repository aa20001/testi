(function (global, factory) {
    const [If, IfStorage] = factory()
    typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = If :
        typeof define === 'function' && define.amd ? define(If) || define(IfStorage) :
            (global = typeof globalThis !== 'undefined' ? globalThis : global || self,
                    global.If = If, global.IfStorage = IfStorage
            );
})(this, (function () {
    'use strict';
    let reveal, config;
    const storageName = "reveal-js-if-plugin";

    const ifStorage = {
        getItems: () => {
            return JSON.parse(sessionStorage.getItem(storageName) || "{}");
        },
        getItem: (key) => {
            return ifStorage.getItems()[key];
        },
        setItem: (key, value) => {
            const old = ifStorage.getItems();
            old[key] = value;
            sessionStorage.setItem(storageName, JSON.stringify(old));
        },
        removeItem: (key) => {
            const old = ifStorage.getItems();
            old[key] = undefined;
            sessionStorage.setItem(storageName, JSON.stringify(old));
        },
        clear: () => {
            sessionStorage.setItem(storageName, "{}");
        }
    }

    const variablesCondition =(condition) => {
        const _variablesCondition = (condition) => {
            switch (condition.type) {
                case "variable":
                    return [condition.variable];
                case "value":
                    return [];
                case "simple":
                    return Object.keys(condition.conditions);
                case "shortcut":
                    return _variablesCondition(config.shortcuts[condition.name])
                case "operation":
                    switch (condition.operation) {
                        case "!":
                            return _variablesCondition(condition.value);
                        case "&&": {
                            const variables = [];
                            for (const cond of condition.values) {
                                if (evalCondition(cond)) {
                                    variables.concat(_variablesCondition(cond));
                                } else {
                                    variables.concat(_variablesCondition(cond));
                                    return variables;
                                }
                            }
                            return variables;
                        }
                        case "||": {
                            if (condition.values.filter(evalCondition).length === 0) return [];
                            return condition.values.filter(evalCondition).map(_variablesCondition).reduce((accumulator, currentValue) => {
                                if (accumulator.length < currentValue.length) {
                                    return accumulator;
                                } else if (accumulator.length > currentValue.length) {
                                    return currentValue;
                                } else {
                                    return [].concat(accumulator, currentValue);
                                }
                            })
                        }
                        case "===":
                        case "!==":
                            return _variablesCondition(condition.left).concat(_variablesCondition(condition.right));
                    }
            }
        }
        return Array.from(new Set(_variablesCondition(condition)))
    }

    const checkVariableCondition = (condition) => {
        /*
        Returns
        -------
        invalid variables
         */
        return variablesCondition(condition).filter(variableName => !(ifStorage.getItem(variableName) && config.variables[variableName].options.map(value => value.value).includes(ifStorage.getItem(variableName))))
    }

    const evalCondition = (condition) => {
        switch (condition.type) {
            case "variable":
                return ifStorage.getItem(condition.variable);
            case "value":
                return condition.value;
            case "simple":
                return Object.keys(condition.conditions).every(key => ifStorage.getItem(key) === condition.conditions[key])
            case "shortcut":
                return evalCondition(config.shortcuts[condition.name])
            case "operation":
                switch (condition.operation) {
                    case "!":
                        return !evalCondition(condition.value);
                    case "&&":
                        return condition.values.map(evalCondition).reduce((accumulator, currentValue) => accumulator && currentValue, true);
                    case "||":
                        return condition.values.map(evalCondition).reduce((accumulator, currentValue) => accumulator || currentValue, false);
                    case "===":
                        return evalCondition(condition.left) === evalCondition(condition.right)
                    case "!==":
                        return evalCondition(condition.left) !== evalCondition(condition.right)
                }
        }
    }

    const set = (event) => {
        let target;
        if (event.target instanceof HTMLSelectElement) {
            target = event.target.selectedOptions[0];
        } else {
            target = event.target;
        }
        if (event) {
            ifStorage.setItem(target.dataset.key, target.dataset.value);
        }
    }
    const setWithReload = (event) => {
        let target;
        if (event.target instanceof HTMLSelectElement) {
            target = event.target.selectedOptions[0];
        } else {
            target = event.target;
        }
        if (event) {
            ifStorage.setItem(target.dataset.key, target.dataset.value);
            window.location.reload();
        }
    }

    const apply = () => {
        reveal.next();
        window.location.reload();
    }

    const createVariableForm = (variable, config_item, reload=false) => {
        const listener = reload?setWithReload:set;
        switch (config_item.type) {
            case "select": {
                const divElem = document.createElement("div");
                divElem.append(config_item.ask || variable, document.createElement("br"));
                const selectElem = document.createElement("select");
                const options = config_item.options.map(option => {
                    const display = option.display || option.value;
                    const optionElem = document.createElement("option");
                    optionElem.textContent = display;
                    optionElem.dataset["key"] = variable;
                    optionElem.dataset["value"] = option.value;
                    if (option.value === ifStorage.getItem(variable)) {
                        optionElem.selected = true;
                    }
                    return optionElem;
                });
                const optionElem = document.createElement("option");
                optionElem.textContent = "";
                optionElem.hidden = true;
                if (!ifStorage.getItem(variable)) {
                    optionElem.selected = true;
                }

                selectElem.append(...options, optionElem);
                selectElem.onchange = listener;
                selectElem.style.fontSize = "0.8em";
                divElem.append(selectElem);
                return divElem
            }
            case "radio": {
                const divElem = document.createElement("div");
                divElem.append(config_item.ask || variable, document.createElement("br"));
                config_item.options.map(option => {
                    const display = option.display || option.value;
                    const optionParent = document.createElement("label")
                    const optionElem = document.createElement("input");
                    optionElem.type = "radio"
                    optionElem.name = variable;
                    optionElem.dataset["key"] = variable;
                    optionElem.dataset["value"] = option.value;
                    if (option.value === ifStorage.getItem(variable)) {
                        optionElem.checked = true;
                    }
                    optionElem.onclick = listener;
                    optionParent.append(optionElem);
                    optionParent.append(display);
                    optionParent.style.padding = "10px 10px"
                    divElem.append(optionParent);
                });
                return divElem
            }
        }

    }

    const Plugin = () => {

        const init = (deck) => {
            reveal = deck
            config = deck.getConfig().If || {}
            deck.getSlidesElement().querySelectorAll("[data-key][data-value]").forEach(value => {
                value.onclick = set;
            });
            deck.getSlidesElement().querySelectorAll("[data-if-apply]").forEach(value => {
                value.onclick = apply;
            });
            deck.getSlidesElement().querySelectorAll("[data-if-conditions]").forEach(value => {
                value.style.display = "none";
            })

            deck.getSlidesElement().querySelectorAll("[data-if-set]").forEach(value => {
                if (!config.variables[value.dataset["ifSet"]]) return;
                value.replaceWith(createVariableForm(value.dataset["ifSet"], config.variables[value.dataset["ifSet"]]));
            });

            deck.getSlidesElement().querySelectorAll("[data-if-setWithReload]").forEach(value => {
                if (!config.variables[value.dataset["ifSetwithreload"]]) return;
                value.replaceWith(createVariableForm(value.dataset["ifSetwithreload"], config.variables[value.dataset["ifSetwithreload"]], true));
            });

            deck.getSlidesElement().querySelectorAll("[data-if]").forEach((value) => {
                let conditions = [];
                const condition = value.querySelector("[data-if-conditions]")
                try {
                    conditions = JSON.parse(condition.textContent || condition.value);
                    condition.style.display = "none";
                } catch (error) {
                    console.error(error);
                    console.error(condition);
                    return;
                }
                if (checkVariableCondition(conditions).length) {
                    const variable = checkVariableCondition(conditions)[0];
                    console.log(checkVariableCondition(conditions));
                    value.innerHTML = "";
                    value.appendChild(createVariableForm(variable, config.variables[variable], true));
                } else {
                    if (!evalCondition(conditions)) {
                        value.dataset["visibility"] = "hidden";
                        value.style.display = "none";
                    } else {
                        value.dataset["visibility"] = "";
                        value.style.display = "";
                    }
                }

            });
        };

        return {
            id: 'if',
            init: init
        };
    };

    return [Plugin, ifStorage];
}));

/*
config:
  If: {
    variables: {
      os: {type: "select", ask: "使用しているOSを選択してください", options: [
        {value: "windows", display: "Windows10"},
        {value: "windows", display: "Windows11"},
        {value: "unix", display: "unix"},
      ]},
      os1: {type: "select", layout: "vertical", ask: "使用しているOSを選択してください", options: [
        {value: "windows", display: "Windows10"},
        {value: "windows", display: "Windows11"},
        {value: "unix", display: "unix"},
      ]}
    },
    shortcuts: {
      isWindows: {"type": "operation", "operation": "||", "values": [{"type": "simple", "conditions": {"os": "windows10"}}, {"type": "simple", "conditions": {"os": "windows11"}}]}
    }
  }
 */

/*
{type: "operation", operation: "and", values: [{type: operation, operation: "===", left: {type: variable, variable: "os"}, right: {type: value, value: "windows"}}, {type: operation, operation: "===", left: {type: variable, variable: "version"}, right: {type: value, value: 11}}]}
{type: "simple", conditions: {os: "windows", version: 11}}

条件に記載された物は最終的に値として解釈されます。
list of types
{type: "simple", conditions: {os: "windows", version: 11}}: conditions内のキーにあたる変数の値と値がすべて等しいときにTrueになります
{type; "value", value: value}: valueとして解釈されます。
{type; "variable", variable: "variable"}: variableの値として解釈されます。
{type: "operation", ...}: 下に別記するが内容についてはなんとなく理解できるはず

list of operations
{type: "operation"", operation: "===", left: {type: "variable", variable: "os"}, right: {type: "value", value: "windows"}}
{type: "operation", operation: "!==", left: {type: "variable", variable: "os"}, right: {type: "value", value: "windows"}}
{type: "operation", operation: "&&", values: [...values]}
{type: "operation", operation: "||", values: [...values]}
{type: "operation", operation: "!", value: {type: "operation", operation: "===", left: {type: "variable", variable: "os"}, right: {type: "value", value: "windows"}}}


 */












